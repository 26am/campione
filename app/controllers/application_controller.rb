class ApplicationController < ActionController::Base
  protect_from_forgery
  protected
  def check_retail_login
    unless session[:retail_user_id] && @retail_user = RetailUser.find(session[:retail_user_id])
      redirect_to(new_retail_login_path) and return false
    end
  end
end
