# require "bundler/capistrano"
# This defines a deployment "recipe" that you can feed to capistrano
# (http://manuals.rubyonrails.com/read/book/17). It allows you to automate
# (among other things) the deployment of your application.

# =============================================================================
# REQUIRED VARIABLES
# =============================================================================
# You must always specify the application and repository for every recipe. The
# repository must be the URL of the repository you want this recipe to
# correspond to. The deploy_to path must be the path on each machine that will
# form the root of the application path.

set :application, "josephcampione.com"
# set :repository, "http://svn.uscm.org/#{application}/trunk"
set :repository,  "git@bitbucket.org:26am/campione.git"
# set :checkout, 'co'
set :keep_releases, '3'


# =============================================================================
# ROLES
# =============================================================================
# You can define any number of roles, each of which contains any number of
# machines. Roles might include such things as :web, or :app, or :db, defining
# what the purpose of each machine is. You can also specify options that can
# be used to single out a specific subset of boxes in a particular role, like
# :primary => true.
# set :target, ENV['target'] || ENV['TARGET'] || 'dev'
default_run_options[:pty] = true
set :scm, "git"
role :db, "campione.26am.com", :primary => true
role :web, "campione.26am.com"
role :app, "campione.26am.com"
set :deploy_to, "/var/www/josephcampione.com"


# define the restart task
desc "Restart the web server"
deploy.task :restart, :roles => :app do
    run "touch #{current_path}/tmp/restart.txt"
end


# =============================================================================
# SSH OPTIONS
# =============================================================================
# ssh_options[:keys] = %w(/path/to/my/key /path/to/another/key)
ssh_options[:forward_agent] = true

set :branch, "master"
set :deploy_via, :remote_cache
set :git_enable_submodules, true
# =============================================================================
# TASKS
# =============================================================================
# Define tasks that run on all (or only some) of the machines. You can specify
# a role (or set of roles) that each task should be executed on. You can also
# narrow the set of servers to a subset of a role by specifying options, which
# must match the options given for the servers to select (like :primary => true)

after 'deploy:update_code', 'local_changes'
desc "Add linked files after deploy and set permissions"
task :local_changes, :roles => :app do
  run <<-CMD
    ln -s #{shared_path}/config/database.yml #{release_path}/config/database.yml
  CMD
  sudo "chown -R apache #{release_path}/tmp"
  sudo "chmod -R g+w #{release_path}/tmp"
end


# You can use "transaction" to indicate that if any of the tasks within it fail,
# all should be rolled back (for each task that specifies an on_rollback
# handler).

desc "Automatically run cleanup"

after 'deploy', 'deploy:cleanup'


after 'deploy:update_code' do
  bundle_cmd     = fetch(:bundle_cmd, "bundle")
  bundle_flags   = fetch(:bundle_flags, "--deployment --quiet")
  bundle_dir     = fetch(:bundle_dir, File.join(fetch(:shared_path), 'bundle'))
  bundle_gemfile = fetch(:bundle_gemfile, "Gemfile")
  bundle_without = [*fetch(:bundle_without, [:development, :test])].compact

  args = ["--gemfile #{File.join(fetch(:current_release), bundle_gemfile)}"]
  args << "--path #{bundle_dir}" unless bundle_dir.to_s.empty?
  args << bundle_flags.to_s
  args << "--without #{bundle_without.join(" ")}" unless bundle_without.empty?


  run_cmd = "cd \"#{fetch(:current_release)}\"; "
  run_cmd << "#{bundle_cmd} install #{args.join(' ')}"

  run run_cmd, :shell => '/bin/bash'
end

# namespace :deploy do
#   desc 'Bundle and minify the JS and CSS files'
#   task :precache_assets, :roles => :app do
#     root_path = File.expand_path(File.dirname(__FILE__) + '/..')
#     assets_path = "#{root_path}/public/packages"
#     gem_path = ENV['GEM_PATH']
#     run_locally "jammit"
#     top.upload assets_path, "#{current_release}/public", :via => :scp, :recursive => true
#   end
# end
# after 'deploy:symlink', 'deploy:precache_assets'

# require 'config/boot'
# require 'hoptoad_notifier/capistrano'
