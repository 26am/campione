class AddHeaderImageToProduct < ActiveRecord::Migration
  def self.up
    add_column :products, :header_image_id, :integer
  end

  def self.down
    remove_column :products, :header_image
  end
end
