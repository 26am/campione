class CreateLibraryImages < ActiveRecord::Migration

  def self.up
    create_table :library_images do |t|
      t.integer :image_id
      t.text :description
      t.string :title
      t.integer :position

      t.timestamps
    end

    add_index :library_images, :id

    load(Rails.root.join('db', 'seeds', 'library_images.rb'))
  end

  def self.down
    UserPlugin.destroy_all({:name => "library_images"})

    Page.delete_all({:link_url => "/library_images"})

    drop_table :library_images
  end

end
