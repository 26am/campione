class CreateRetailUsers < ActiveRecord::Migration

  def self.up
    create_table :retail_users do |t|
      t.string :name
      t.string :account_no
      t.string :password
      t.integer :position

      t.timestamps
    end

    add_index :retail_users, :id

    load(Rails.root.join('db', 'seeds', 'retail_users.rb'))
  end

  def self.down
    UserPlugin.destroy_all({:name => "retail_users"})

    Page.delete_all({:link_url => "/retail_users"})

    drop_table :retail_users
  end

end
