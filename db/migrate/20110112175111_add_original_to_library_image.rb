class AddOriginalToLibraryImage < ActiveRecord::Migration
  def self.up
    add_column :library_images, :original_id, :integer
  end

  def self.down
    remove_column :library_images, :original_id
  end
end
