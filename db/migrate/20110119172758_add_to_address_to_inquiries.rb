class AddToAddressToInquiries < ActiveRecord::Migration
  def self.up
    add_column :inquiries, :to_address, :string
  end

  def self.down
    remove_column :inquiries, :to_address
  end
end
