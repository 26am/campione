# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20110120005536) do

  create_table "images", :force => true do |t|
    t.string   "image_mime_type"
    t.string   "image_name"
    t.integer  "image_size"
    t.integer  "image_width"
    t.integer  "image_height"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_uid"
    t.string   "image_ext"
  end

  create_table "inquiries", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.text     "message"
    t.integer  "position"
    t.boolean  "open",       :default => true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "spam",       :default => false
    t.string   "to_address"
  end

  create_table "inquiry_settings", :force => true do |t|
    t.string   "name"
    t.text     "value"
    t.boolean  "destroyable"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "library_images", :force => true do |t|
    t.integer  "image_id"
    t.text     "description"
    t.string   "title"
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "original_id"
  end

  add_index "library_images", ["id"], :name => "index_library_images_on_id"

  create_table "library_templates", :force => true do |t|
    t.string   "title"
    t.integer  "preview_image_id"
    t.integer  "file_id"
    t.text     "description"
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "library_templates", ["id"], :name => "index_library_templates_on_id"

  create_table "page_part_translations", :force => true do |t|
    t.integer  "page_part_id"
    t.string   "locale"
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "page_part_translations", ["page_part_id"], :name => "index_page_part_translations_on_page_part_id"

  create_table "page_parts", :force => true do |t|
    t.integer  "page_id"
    t.string   "title"
    t.text     "body"
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "page_parts", ["id"], :name => "index_page_parts_on_id"
  add_index "page_parts", ["page_id"], :name => "index_page_parts_on_page_id"

  create_table "page_translations", :force => true do |t|
    t.integer  "page_id"
    t.string   "locale"
    t.string   "title"
    t.string   "meta_keywords"
    t.text     "meta_description"
    t.string   "browser_title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "page_translations", ["page_id"], :name => "index_page_translations_on_page_id"

  create_table "pages", :force => true do |t|
    t.string   "title"
    t.integer  "parent_id"
    t.integer  "position"
    t.string   "path"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "meta_keywords"
    t.text     "meta_description"
    t.boolean  "show_in_menu",        :default => true
    t.string   "link_url"
    t.string   "menu_match"
    t.boolean  "deletable",           :default => true
    t.string   "custom_title"
    t.string   "custom_title_type",   :default => "none"
    t.boolean  "draft",               :default => false
    t.string   "browser_title"
    t.boolean  "skip_to_first_child", :default => false
    t.integer  "lft"
    t.integer  "rgt"
    t.integer  "depth"
  end

  add_index "pages", ["depth"], :name => "index_pages_on_depth"
  add_index "pages", ["id"], :name => "index_pages_on_id"
  add_index "pages", ["lft"], :name => "index_pages_on_lft"
  add_index "pages", ["parent_id"], :name => "index_pages_on_parent_id"
  add_index "pages", ["rgt"], :name => "index_pages_on_rgt"

  create_table "product_recipes", :force => true do |t|
    t.integer  "product_id"
    t.integer  "recipe_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "products", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.text     "cooking_instructions"
    t.text     "product_variations"
    t.text     "retailer_information"
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "header_image_id"
    t.boolean  "featured",             :default => false
    t.integer  "featured_image_id"
  end

  add_index "products", ["id"], :name => "index_products_on_id"

  create_table "recipes", :force => true do |t|
    t.string   "title"
    t.text     "recipe"
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "recipes", ["id"], :name => "index_recipes_on_id"

  create_table "refinery_settings", :force => true do |t|
    t.string   "name"
    t.text     "value"
    t.boolean  "destroyable",             :default => true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "scoping"
    t.boolean  "restricted",              :default => false
    t.string   "callback_proc_as_string"
    t.string   "form_value_type"
  end

  add_index "refinery_settings", ["name"], :name => "index_refinery_settings_on_name"

  create_table "resources", :force => true do |t|
    t.string   "file_mime_type"
    t.string   "file_name"
    t.integer  "file_size"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "file_uid"
    t.string   "file_ext"
  end

  create_table "retail_users", :force => true do |t|
    t.string   "name"
    t.string   "account_no"
    t.string   "password"
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "retail_users", ["id"], :name => "index_retail_users_on_id"

  create_table "roles", :force => true do |t|
    t.string "title"
  end

  create_table "roles_users", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "roles_users", ["role_id", "user_id"], :name => "index_roles_users_on_role_id_and_user_id"
  add_index "roles_users", ["user_id", "role_id"], :name => "index_roles_users_on_user_id_and_role_id"

  create_table "slugs", :force => true do |t|
    t.string   "name"
    t.integer  "sluggable_id"
    t.integer  "sequence",                     :default => 1, :null => false
    t.string   "sluggable_type", :limit => 40
    t.string   "scope",          :limit => 40
    t.datetime "created_at"
    t.string   "locale"
  end

  add_index "slugs", ["locale"], :name => "index_slugs_on_locale"
  add_index "slugs", ["name", "sluggable_type", "scope", "sequence"], :name => "index_slugs_on_name_and_sluggable_type_and_scope_and_sequence", :unique => true
  add_index "slugs", ["sluggable_id"], :name => "index_slugs_on_sluggable_id"

  create_table "ssp_account", :force => true do |t|
    t.boolean  "externals"
    t.boolean  "internals"
    t.string   "version"
    t.string   "activation_key"
    t.datetime "last_check"
    t.integer  "last_schedule_check"
    t.string   "theme",               :default => "/app/webroot/styles/default/default.css"
    t.string   "lang",                :default => "eng"
    t.string   "api_key"
    t.boolean  "grace",               :default => false
    t.string   "db_version",          :default => "1520"
    t.integer  "archive_w"
    t.text     "caption_template"
    t.string   "title_template"
    t.text     "link_template"
    t.boolean  "first_time",          :default => false
  end

  create_table "ssp_albums", :force => true do |t|
    t.string   "name",             :limit => 100
    t.text     "description"
    t.string   "path",             :limit => 50
    t.boolean  "tn",                              :default => false,    :null => false
    t.string   "aTn",              :limit => 150
    t.boolean  "active",                          :default => false,    :null => false
    t.string   "audioFile",        :limit => 100
    t.string   "audioCap",         :limit => 200
    t.integer  "displayOrder",                    :default => 999
    t.integer  "target",                          :default => 0,        :null => false
    t.integer  "images_count",                    :default => 0,        :null => false
    t.integer  "video_count",                     :default => 0,        :null => false
    t.string   "sort_type",                       :default => "manual", :null => false
    t.string   "title_template"
    t.text     "link_template"
    t.text     "caption_template"
    t.datetime "modified"
    t.datetime "created"
    t.integer  "created_on"
    t.integer  "modified_on"
    t.integer  "updated_by"
    t.integer  "created_by"
    t.boolean  "smart",                           :default => false
    t.text     "smart_query"
    t.string   "place_taken"
    t.string   "date_taken",       :limit => 20
    t.integer  "preview_id",                      :default => 0
    t.text     "tags"
    t.integer  "watermark_id"
    t.string   "internal_id",      :limit => 32
  end

  add_index "ssp_albums", ["active"], :name => "active"
  add_index "ssp_albums", ["created_by"], :name => "created_by"
  add_index "ssp_albums", ["created_on"], :name => "created_on"
  add_index "ssp_albums", ["images_count"], :name => "images_count"
  add_index "ssp_albums", ["modified_on"], :name => "modified_on"
  add_index "ssp_albums", ["updated_by"], :name => "updated_by"
  add_index "ssp_albums", ["watermark_id"], :name => "watermark_id"

  create_table "ssp_dynamic", :force => true do |t|
    t.string   "name",        :limit => 100
    t.text     "description"
    t.datetime "modified"
    t.datetime "created"
    t.integer  "created_on"
    t.integer  "modified_on"
    t.boolean  "main",                       :default => false
    t.string   "sort_type",                  :default => "manual", :null => false
    t.integer  "updated_by"
    t.integer  "created_by"
    t.integer  "tag_count",                  :default => 0
    t.boolean  "smart",                      :default => false
    t.text     "smart_query"
    t.string   "internal_id", :limit => 32
  end

  add_index "ssp_dynamic", ["created_by"], :name => "created_by"
  add_index "ssp_dynamic", ["created_on"], :name => "created_on"
  add_index "ssp_dynamic", ["main"], :name => "main"
  add_index "ssp_dynamic", ["modified_on"], :name => "modified_on"
  add_index "ssp_dynamic", ["tag_count"], :name => "tag_count"
  add_index "ssp_dynamic", ["updated_by"], :name => "updated_by"

  create_table "ssp_dynamic_links", :force => true do |t|
    t.integer "did"
    t.integer "aid"
    t.integer "display", :default => 800
  end

  add_index "ssp_dynamic_links", ["aid"], :name => "aid"
  add_index "ssp_dynamic_links", ["did"], :name => "did"
  add_index "ssp_dynamic_links", ["display"], :name => "display"

  create_table "ssp_images", :force => true do |t|
    t.integer  "aid"
    t.string   "title"
    t.string   "src"
    t.text     "caption"
    t.text     "link"
    t.boolean  "active",        :default => true,  :null => false
    t.integer  "seq",           :default => 999,   :null => false
    t.integer  "pause",         :default => 0,     :null => false
    t.integer  "target",        :default => 0,     :null => false
    t.datetime "modified"
    t.datetime "created"
    t.integer  "created_on"
    t.integer  "modified_on"
    t.integer  "updated_by"
    t.integer  "created_by"
    t.string   "anchor"
    t.integer  "filesize"
    t.text     "tags"
    t.integer  "captured_on"
    t.boolean  "is_video",      :default => false
    t.integer  "start_on"
    t.integer  "end_on"
    t.string   "lg_preview"
    t.integer  "lg_preview_id"
    t.string   "tn_preview"
    t.integer  "tn_preview_id"
    t.boolean  "album_active",  :default => false
  end

  add_index "ssp_images", ["aid"], :name => "aid"
  add_index "ssp_images", ["album_active"], :name => "album_active"
  add_index "ssp_images", ["captured_on"], :name => "captured_on"
  add_index "ssp_images", ["created_by"], :name => "created_by"
  add_index "ssp_images", ["created_on"], :name => "created_on"
  add_index "ssp_images", ["end_on"], :name => "end_on"
  add_index "ssp_images", ["modified_on"], :name => "modified_on"
  add_index "ssp_images", ["seq"], :name => "seq"
  add_index "ssp_images", ["src"], :name => "src"
  add_index "ssp_images", ["start_on"], :name => "start_on"
  add_index "ssp_images", ["updated_by"], :name => "updated_by"

  create_table "ssp_slideshows", :force => true do |t|
    t.string "name"
    t.string "url"
  end

  create_table "ssp_usrs", :force => true do |t|
    t.string   "usr",          :limit => 50
    t.string   "pwd",          :limit => 50
    t.string   "email"
    t.integer  "perms",                      :default => 1,    :null => false
    t.datetime "modified"
    t.datetime "created"
    t.integer  "created_on"
    t.integer  "modified_on"
    t.boolean  "news",                       :default => true
    t.boolean  "help",                       :default => true
    t.string   "display_name"
    t.integer  "last_seen"
    t.string   "first_name"
    t.string   "last_name"
    t.text     "profile"
    t.text     "externals"
    t.string   "anchor"
    t.string   "theme"
    t.string   "lang"
  end

  add_index "ssp_usrs", ["usr"], :name => "usr"

  create_table "ssp_watermarks", :force => true do |t|
    t.string  "name"
    t.string  "fn"
    t.integer "position", :default => 5
    t.boolean "main",     :default => false
    t.integer "opacity",  :default => 60
  end

  add_index "ssp_watermarks", ["main"], :name => "main"

  create_table "stores", :force => true do |t|
    t.string   "title"
    t.string   "states",     :limit => 1000
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "stores", ["id"], :name => "index_stores_on_id"

  create_table "user_plugins", :force => true do |t|
    t.integer "user_id"
    t.string  "name"
    t.integer "position"
  end

  add_index "user_plugins", ["name"], :name => "index_user_plugins_on_title"
  add_index "user_plugins", ["user_id", "name"], :name => "index_unique_user_plugins", :unique => true

  create_table "users", :force => true do |t|
    t.string   "username",             :null => false
    t.string   "email",                :null => false
    t.string   "encrypted_password",   :null => false
    t.string   "password_salt",        :null => false
    t.string   "persistence_token"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "perishable_token"
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "sign_in_count"
    t.string   "remember_token"
    t.string   "reset_password_token"
    t.datetime "remember_created_at"
  end

  add_index "users", ["id"], :name => "index_users_on_id"

end
