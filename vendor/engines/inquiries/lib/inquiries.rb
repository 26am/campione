require 'refinery'
require 'filters_spam'

module Refinery
  module Inquiries
    class Engine < Rails::Engine
      # initializer "static assets" do |app|
      #   app.middleware.insert_after ::ActionDispatch::Static, ::ActionDispatch::Static, "#{root}/public"
      # end
      config.after_initialize do
        Refinery::Plugin.register do |plugin|
          plugin.name = "inquiries"
          plugin.activity = {:class => Inquiry}
        end
      end
    end
  end
end
