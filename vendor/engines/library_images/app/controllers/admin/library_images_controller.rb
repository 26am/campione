class Admin::LibraryImagesController < Admin::BaseController

  crudify :library_image
  
  def index
    @library_images = LibraryImage.order(:position).paginate(:per_page => 1000, :page => params[:page])
  end

end
