class LibraryImagesController < ApplicationController
  before_filter :check_retail_login
  
  before_filter :find_all_library_images, :only => :index
  before_filter :find_page, :except => :download

  def index
    # you can use meta fields from your model instead (e.g. browser_title)
    # by swapping @page for @library_image in the line below:
    present(@library_images.first)
  end

  def show
    @library_image = LibraryImage.find(params[:id])

    # you can use meta fields from your model instead (e.g. browser_title)
    # by swapping @page for @library_image in the line below:
    present(@library_image)
  end
  
  def download
    @library_image = LibraryImage.find(params[:id])
    if @library_image.original
      send_file(@library_image.original.file.path, :filename => @library_image.original.file_name)
    else
      send_file(@library_image.image.image.path, :filename => @library_image.image.image_name)
    end
  end

protected

  def find_all_library_images
    @library_images = LibraryImage.order("position ASC").paginate(:page => params[:page])
  end

  def find_page
    @page = Page.find_by_link_url("/library_images")
  end

end
