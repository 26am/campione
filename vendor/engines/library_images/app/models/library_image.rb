class LibraryImage < ActiveRecord::Base

  acts_as_indexed :fields => [:description, :title]
  
  validates_presence_of :title
  validates_uniqueness_of :title
  
  belongs_to :image
  belongs_to :original, :class_name => 'Resource'
  
  def self.title
    'Image Library'
  end
  
  def self.browser_title
    'Image Library'
  end
end
