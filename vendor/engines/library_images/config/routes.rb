Refinery::Application.routes.draw do
  resources :library_images do
    member do
      get :download
    end
  end

  scope(:path => 'refinery', :as => 'admin', :module => 'admin') do
    resources :library_images do
      collection do
        post :update_positions
      end
    end
  end
end
