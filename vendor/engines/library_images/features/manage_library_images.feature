@library_images
Feature: Library Images
  In order to have library_images on my website
  As an administrator
  I want to manage library_images

  Background:
    Given I am a logged in refinery user
    And I have no library_images

  @library_images-list @list
  Scenario: Library Images List
   Given I have library_images titled UniqueTitleOne, UniqueTitleTwo
   When I go to the list of library_images
   Then I should see "UniqueTitleOne"
   And I should see "UniqueTitleTwo"

  @library_images-valid @valid
  Scenario: Create Valid Library Image
    When I go to the list of library_images
    And I follow "Add New Library Image"
    And I fill in "Title" with "This is a test of the first string field"
    And I press "Save"
    Then I should see "'This is a test of the first string field' was successfully added."
    And I should have 1 library_image

  @library_images-invalid @invalid
  Scenario: Create Invalid Library Image (without title)
    When I go to the list of library_images
    And I follow "Add New Library Image"
    And I press "Save"
    Then I should see "Title can't be blank"
    And I should have 0 library_images

  @library_images-edit @edit
  Scenario: Edit Existing Library Image
    Given I have library_images titled "A title"
    When I go to the list of library_images
    And I follow "Edit this library_image" within ".actions"
    Then I fill in "Title" with "A different title"
    And I press "Save"
    Then I should see "'A different title' was successfully updated."
    And I should be on the list of library_images
    And I should not see "A title"

  @library_images-duplicate @duplicate
  Scenario: Create Duplicate Library Image
    Given I only have library_images titled UniqueTitleOne, UniqueTitleTwo
    When I go to the list of library_images
    And I follow "Add New Library Image"
    And I fill in "Title" with "UniqueTitleTwo"
    And I press "Save"
    Then I should see "There were problems"
    And I should have 2 library_images

  @library_images-delete @delete
  Scenario: Delete Library Image
    Given I only have library_images titled UniqueTitleOne
    When I go to the list of library_images
    And I follow "Remove this library image forever"
    Then I should see "'UniqueTitleOne' was successfully removed."
    And I should have 0 library_images
 