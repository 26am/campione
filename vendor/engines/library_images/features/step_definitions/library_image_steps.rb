Given /^I have no library_images$/ do
  LibraryImage.delete_all
end

Given /^I (only )?have library_images titled "?([^"]*)"?$/ do |only, titles|
  LibraryImage.delete_all if only
  titles.split(', ').each do |title|
    LibraryImage.create(:title => title)
  end
end

Then /^I should have ([0-9]+) library_images?$/ do |count|
  LibraryImage.count.should == count.to_i
end
