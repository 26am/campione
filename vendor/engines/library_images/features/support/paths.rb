module NavigationHelpers
  module Refinery
    module LibraryImages
      def path_to(page_name)
        case page_name
        when /the list of library_images/
          admin_library_images_path

         when /the new library_image form/
          new_admin_library_image_path
        else
          nil
        end
      end
    end
  end
end
