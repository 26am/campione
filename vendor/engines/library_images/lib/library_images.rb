require 'refinery'

module Refinery
  module LibraryImages
    class Engine < Rails::Engine
      initializer "static assets" do |app|
        app.middleware.insert_after ::ActionDispatch::Static, ::ActionDispatch::Static, "#{root}/public"
      end

      config.after_initialize do
        Refinery::Plugin.register do |plugin|
          plugin.name = "library_images"
          plugin.activity = {:class => LibraryImage}
        end
      end
    end
  end
end
