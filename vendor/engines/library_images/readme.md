# Library Images engine for Refinery CMS.

## How to build this engine as a gem

    cd vendor/engines/library_images
    gem build refinerycms-library_images.gempspec
    gem install refinerycms-library_images.gem
    
    # Sign up for a http://rubygems.org/ account and publish the gem
    gem push refinerycms-library_images.gem