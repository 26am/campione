class LibraryTemplatesController < ApplicationController
  before_filter :check_retail_login

  before_filter :find_all_library_templates
  before_filter :find_page

  def index
    # you can use meta fields from your model instead (e.g. browser_title)
    # by swapping @page for @library_template in the line below:
    present(@library_templates.first)
  end

  def show
    @library_template = LibraryTemplate.find(params[:id])

    # you can use meta fields from your model instead (e.g. browser_title)
    # by swapping @page for @library_template in the line below:
    present(@library_template)
  end

protected

  def find_all_library_templates
    @library_templates = LibraryTemplate.order("position ASC").paginate(:page => params[:page])
  end

  def find_page
    @page = Page.find_by_link_url("/library_templates")
  end

end
