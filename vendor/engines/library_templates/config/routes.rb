Refinery::Application.routes.draw do
  resources :library_templates

  scope(:path => 'refinery', :as => 'admin', :module => 'admin') do
    resources :library_templates do
      collection do
        post :update_positions
      end
    end
  end
end
