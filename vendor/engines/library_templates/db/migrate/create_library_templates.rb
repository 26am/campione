class CreateLibraryTemplates < ActiveRecord::Migration

  def self.up
    create_table :library_templates do |t|
      t.string :title
      t.integer :preview_image_id
      t.integer :file_id
      t.text :description
      t.integer :position

      t.timestamps
    end

    add_index :library_templates, :id

    load(Rails.root.join('db', 'seeds', 'library_templates.rb'))
  end

  def self.down
    UserPlugin.destroy_all({:name => "library_templates"})

    Page.delete_all({:link_url => "/library_templates"})

    drop_table :library_templates
  end

end
