@library_templates
Feature: Library Templates
  In order to have library_templates on my website
  As an administrator
  I want to manage library_templates

  Background:
    Given I am a logged in refinery user
    And I have no library_templates

  @library_templates-list @list
  Scenario: Library Templates List
   Given I have library_templates titled UniqueTitleOne, UniqueTitleTwo
   When I go to the list of library_templates
   Then I should see "UniqueTitleOne"
   And I should see "UniqueTitleTwo"

  @library_templates-valid @valid
  Scenario: Create Valid Library Template
    When I go to the list of library_templates
    And I follow "Add New Library Template"
    And I fill in "Title" with "This is a test of the first string field"
    And I press "Save"
    Then I should see "'This is a test of the first string field' was successfully added."
    And I should have 1 library_template

  @library_templates-invalid @invalid
  Scenario: Create Invalid Library Template (without title)
    When I go to the list of library_templates
    And I follow "Add New Library Template"
    And I press "Save"
    Then I should see "Title can't be blank"
    And I should have 0 library_templates

  @library_templates-edit @edit
  Scenario: Edit Existing Library Template
    Given I have library_templates titled "A title"
    When I go to the list of library_templates
    And I follow "Edit this library_template" within ".actions"
    Then I fill in "Title" with "A different title"
    And I press "Save"
    Then I should see "'A different title' was successfully updated."
    And I should be on the list of library_templates
    And I should not see "A title"

  @library_templates-duplicate @duplicate
  Scenario: Create Duplicate Library Template
    Given I only have library_templates titled UniqueTitleOne, UniqueTitleTwo
    When I go to the list of library_templates
    And I follow "Add New Library Template"
    And I fill in "Title" with "UniqueTitleTwo"
    And I press "Save"
    Then I should see "There were problems"
    And I should have 2 library_templates

  @library_templates-delete @delete
  Scenario: Delete Library Template
    Given I only have library_templates titled UniqueTitleOne
    When I go to the list of library_templates
    And I follow "Remove this library template forever"
    Then I should see "'UniqueTitleOne' was successfully removed."
    And I should have 0 library_templates
 