Given /^I have no library_templates$/ do
  LibraryTemplate.delete_all
end

Given /^I (only )?have library_templates titled "?([^"]*)"?$/ do |only, titles|
  LibraryTemplate.delete_all if only
  titles.split(', ').each do |title|
    LibraryTemplate.create(:title => title)
  end
end

Then /^I should have ([0-9]+) library_templates?$/ do |count|
  LibraryTemplate.count.should == count.to_i
end
