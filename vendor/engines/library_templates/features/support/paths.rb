module NavigationHelpers
  module Refinery
    module LibraryTemplates
      def path_to(page_name)
        case page_name
        when /the list of library_templates/
          admin_library_templates_path

         when /the new library_template form/
          new_admin_library_template_path
        else
          nil
        end
      end
    end
  end
end
