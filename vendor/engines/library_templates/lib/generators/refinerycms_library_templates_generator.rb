class RefinerycmsLibraryTemplates < Refinery::Generators::EngineInstaller

  source_root File.expand_path('../../', __FILE__)
  engine_name "library_templates"

end
