# Library Templates engine for Refinery CMS.

## How to build this engine as a gem

    cd vendor/engines/library_templates
    gem build refinerycms-library_templates.gempspec
    gem install refinerycms-library_templates.gem
    
    # Sign up for a http://rubygems.org/ account and publish the gem
    gem push refinerycms-library_templates.gem