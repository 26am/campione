class Product < ActiveRecord::Base

  acts_as_indexed :fields => [:title, :description, :cooking_instructions, :product_variations, :retailer_information]
  has_friendly_id :title, :use_slug => true
  
  belongs_to :header_image, :class_name => 'Image'
  belongs_to :featured_image, :class_name => 'Image'
  
  has_many :product_recipes
  has_many :recipes, :through => :product_recipes
  
  validates_presence_of :title
  validates_uniqueness_of :title
  
  after_create :create_menu_item
  
  scope :featured, where(:featured => true)
  
  def create_menu_item
    Page.create!(:title => title, :link_url => '/products/' + slug.name, :parent_id => 11)
  end
end
