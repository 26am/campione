Refinery::Application.routes.draw do
  match 'site' => "site#index"
  resources :products

  scope(:path => 'refinery', :as => 'admin', :module => 'admin') do
    resources :products do
      collection do
        post :update_positions
      end
    end
  end
end
