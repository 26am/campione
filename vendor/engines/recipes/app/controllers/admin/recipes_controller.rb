class Admin::RecipesController < Admin::BaseController

  crudify :recipe

  def update
    if @recipe.update_attributes(params[:recipe])
      flash.notice = t(
        'refinery.crudify.updated',
        :what => "'#{@recipe.title}'"
      )
      if params[:recipe_products].present?
        @recipe.product_recipes.destroy_all
        params[:recipe_products].each do |p_id|
          ProductRecipe.create(:product_id => p_id, :recipe_id => @recipe.id)
        end
      end
      unless params[:continue_editing] =~ /true|on|1/
        redirect_back_or_default(admin_recipes_path)
      else
        redirect_to :back
      end
    else
      render :action => 'edit'
    end
  end

  def create
    if (@recipe = Recipe.create(params[:recipe])).valid?
      flash.notice = t(
        'refinery.crudify.created',
        :what => "'#{@recipe.title}'"
      )
      if params[:recipe_products].present?
        params[:recipe_products].each do |p_id|
          ProductRecipe.create(:product_id => p_id, :recipe_id => @recipe.id)
        end
      end
      unless params[:continue_editing] =~ /true|on|1/
        redirect_back_or_default(admin_recipes_path)
      else
        redirect_to :back
      end
    else
      render :action => 'new'
    end
  end
end
