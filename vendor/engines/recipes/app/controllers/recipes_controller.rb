class RecipesController < ApplicationController

  before_filter :find_all_recipes
  before_filter :find_page

  def index
    # you can use meta fields from your model instead (e.g. browser_title)
    # by swapping @page for @recipe in the line below:
    present(@page)
  end

  def show
    @recipe = Recipe.find(params[:id])

    # you can use meta fields from your model instead (e.g. browser_title)
    # by swapping @page for @recipe in the line below:
    present(@page)
  end

protected

  def find_all_recipes
    @recipes = Recipe.find(:all, :order => "position ASC")
  end

  def find_page
    @page = Page.find_by_link_url("/recipes")
  end

end
