class Recipe < ActiveRecord::Base

  acts_as_indexed :fields => [:recipe]
  
  has_many :product_recipes
  has_many :products, :through => :product_recipes
  def to_s
    title
  end
end
