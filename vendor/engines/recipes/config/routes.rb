Refinery::Application.routes.draw do
  resources :recipes

  scope(:path => 'refinery', :as => 'admin', :module => 'admin') do
    resources :recipes do
      collection do
        post :update_positions
      end
    end
  end
end
