class CreateRecipes < ActiveRecord::Migration

  def self.up
    create_table :recipes do |t|
      t.string :title
      t.text :recipe
      t.integer :position

      t.timestamps
    end

    add_index :recipes, :id

    load(Rails.root.join('db', 'seeds', 'recipes.rb'))
  end

  def self.down
    UserPlugin.destroy_all({:name => "recipes"})

    Page.delete_all({:link_url => "/recipes"})

    drop_table :recipes
  end

end
