@recipes
Feature: Recipes
  In order to have recipes on my website
  As an administrator
  I want to manage recipes

  Background:
    Given I am a logged in refinery user
    And I have no recipes
