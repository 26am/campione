Given /^I have no recipes$/ do
  Recipe.delete_all
end


Then /^I should have ([0-9]+) recipes?$/ do |count|
  Recipe.count.should == count.to_i
end
