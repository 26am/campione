module NavigationHelpers
  module Refinery
    module Recipes
      def path_to(page_name)
        case page_name
        when /the list of recipes/
          admin_recipes_path

         when /the new recipe form/
          new_admin_recipe_path
        else
          nil
        end
      end
    end
  end
end
