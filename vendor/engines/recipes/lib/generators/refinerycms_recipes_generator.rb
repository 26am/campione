class RefinerycmsRecipes < Refinery::Generators::EngineInstaller

  source_root File.expand_path('../../', __FILE__)
  engine_name "recipes"

end
