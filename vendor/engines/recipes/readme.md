# Recipes engine for Refinery CMS.

## How to build this engine as a gem

    cd vendor/engines/recipes
    gem build refinerycms-recipes.gempspec
    gem install refinerycms-recipes.gem
    
    # Sign up for a http://rubygems.org/ account and publish the gem
    gem push refinerycms-recipes.gem