Gem::Specification.new do |s|
  s.platform          = Gem::Platform::RUBY
  s.name              = 'refinerycms-recipes'
  s.version           = '1.0'
  s.description       = 'Ruby on Rails Recipes engine for Refinery CMS'
  s.date              = '2010-11-27'
  s.summary           = 'Recipes engine for Refinery CMS'
  s.require_paths     = %w(lib)
  s.files             = Dir['lib/**/*', 'config/**/*', 'app/**/*']
end
