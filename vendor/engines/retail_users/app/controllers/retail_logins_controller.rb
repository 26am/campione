class RetailLoginsController < ApplicationController

  def new
    @account_no = cookies[:account_no]
    present(@page)
  end

  def create
    if params[:save_account_no] == '1'
      cookies.permanent[:account_no] = params[:account_no] 
    else
      cookies.delete(:account_no)
    end
    if @retail_user = RetailUser.find_by_account_no_and_password(params[:account_no], params[:password])
      session[:retail_user_id] = @retail_user.id
      redirect_to(library_images_path) and return
    end
    @account_no = params[:account_no]
    render :new
  end

end
