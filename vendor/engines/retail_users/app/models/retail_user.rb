class RetailUser < ActiveRecord::Base

  acts_as_indexed :fields => [:name, :account_no, :password]
  
  validates_presence_of :name, :account_no, :password
  validates_uniqueness_of :name, :account_no
  
  def to_s
    name
  end
  
end
