Refinery::Application.routes.draw do
  resources :retail_logins

  scope(:path => 'refinery', :as => 'admin', :module => 'admin') do
    resources :retail_users do
      collection do
        post :update_positions
      end
    end
  end
end
