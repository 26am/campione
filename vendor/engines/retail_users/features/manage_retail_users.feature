@retail_users
Feature: Retail Users
  In order to have retail_users on my website
  As an administrator
  I want to manage retail_users

  Background:
    Given I am a logged in refinery user
    And I have no retail_users

  @retail_users-list @list
  Scenario: Retail Users List
   Given I have retail_users titled UniqueTitleOne, UniqueTitleTwo
   When I go to the list of retail_users
   Then I should see "UniqueTitleOne"
   And I should see "UniqueTitleTwo"

  @retail_users-valid @valid
  Scenario: Create Valid Retail User
    When I go to the list of retail_users
    And I follow "Add New Retail User"
    And I fill in "Name" with "This is a test of the first string field"
    And I press "Save"
    Then I should see "'This is a test of the first string field' was successfully added."
    And I should have 1 retail_user

  @retail_users-invalid @invalid
  Scenario: Create Invalid Retail User (without name)
    When I go to the list of retail_users
    And I follow "Add New Retail User"
    And I press "Save"
    Then I should see "Name can't be blank"
    And I should have 0 retail_users

  @retail_users-edit @edit
  Scenario: Edit Existing Retail User
    Given I have retail_users titled "A name"
    When I go to the list of retail_users
    And I follow "Edit this retail_user" within ".actions"
    Then I fill in "Name" with "A different name"
    And I press "Save"
    Then I should see "'A different name' was successfully updated."
    And I should be on the list of retail_users
    And I should not see "A name"

  @retail_users-duplicate @duplicate
  Scenario: Create Duplicate Retail User
    Given I only have retail_users titled UniqueTitleOne, UniqueTitleTwo
    When I go to the list of retail_users
    And I follow "Add New Retail User"
    And I fill in "Name" with "UniqueTitleTwo"
    And I press "Save"
    Then I should see "There were problems"
    And I should have 2 retail_users

  @retail_users-delete @delete
  Scenario: Delete Retail User
    Given I only have retail_users titled UniqueTitleOne
    When I go to the list of retail_users
    And I follow "Remove this retail user forever"
    Then I should see "'UniqueTitleOne' was successfully removed."
    And I should have 0 retail_users
 