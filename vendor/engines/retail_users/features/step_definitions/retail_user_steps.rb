Given /^I have no retail_users$/ do
  RetailUser.delete_all
end

Given /^I (only )?have retail_users titled "?([^"]*)"?$/ do |only, titles|
  RetailUser.delete_all if only
  titles.split(', ').each do |title|
    RetailUser.create(:name => title)
  end
end

Then /^I should have ([0-9]+) retail_users?$/ do |count|
  RetailUser.count.should == count.to_i
end
