module NavigationHelpers
  module Refinery
    module RetailUsers
      def path_to(page_name)
        case page_name
        when /the list of retail_users/
          admin_retail_users_path

         when /the new retail_user form/
          new_admin_retail_user_path
        else
          nil
        end
      end
    end
  end
end
