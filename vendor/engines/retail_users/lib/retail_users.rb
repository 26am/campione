require 'refinery'

module Refinery
  module RetailUsers
    class Engine < Rails::Engine
      initializer "static assets" do |app|
        app.middleware.insert_after ::ActionDispatch::Static, ::ActionDispatch::Static, "#{root}/public"
      end

      config.after_initialize do
        Refinery::Plugin.register do |plugin|
          plugin.name = "retail_users"
          plugin.activity = {:class => RetailUser,
          :title => 'name'
        }
        end
      end
    end
  end
end
