# Retail Users engine for Refinery CMS.

## How to build this engine as a gem

    cd vendor/engines/retail_users
    gem build refinerycms-retail_users.gempspec
    gem install refinerycms-retail_users.gem
    
    # Sign up for a http://rubygems.org/ account and publish the gem
    gem push refinerycms-retail_users.gem