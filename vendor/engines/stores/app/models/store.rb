class Store < ActiveRecord::Base

  acts_as_indexed :fields => [:title, :states]
  
  validates_presence_of :title
  validates_uniqueness_of :title
  
  serialize :states
  
end
