Refinery::Application.routes.draw do
  resources :stores

  scope(:path => 'refinery', :as => 'admin', :module => 'admin') do
    resources :stores do
      collection do
        post :update_positions
      end
    end
  end
end
