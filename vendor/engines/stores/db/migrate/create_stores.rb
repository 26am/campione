class CreateStores < ActiveRecord::Migration

  def self.up
    create_table :stores do |t|
      t.string :title
      t.string :states,          :limit => 1000
      t.integer :position

      t.timestamps
    end

    add_index :stores, :id

    load(Rails.root.join('db', 'seeds', 'stores.rb'))
  end

  def self.down
    UserPlugin.destroy_all({:name => "stores"})

    Page.delete_all({:link_url => "/stores"})

    drop_table :stores
  end

end
