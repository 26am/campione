@stores
Feature: Stores
  In order to have stores on my website
  As an administrator
  I want to manage stores

  Background:
    Given I am a logged in refinery user
    And I have no stores

  @stores-list @list
  Scenario: Stores List
   Given I have stores titled UniqueTitleOne, UniqueTitleTwo
   When I go to the list of stores
   Then I should see "UniqueTitleOne"
   And I should see "UniqueTitleTwo"

  @stores-valid @valid
  Scenario: Create Valid Store
    When I go to the list of stores
    And I follow "Add New Store"
    And I fill in "Title" with "This is a test of the first string field"
    And I press "Save"
    Then I should see "'This is a test of the first string field' was successfully added."
    And I should have 1 store

  @stores-invalid @invalid
  Scenario: Create Invalid Store (without title)
    When I go to the list of stores
    And I follow "Add New Store"
    And I press "Save"
    Then I should see "Title can't be blank"
    And I should have 0 stores

  @stores-edit @edit
  Scenario: Edit Existing Store
    Given I have stores titled "A title"
    When I go to the list of stores
    And I follow "Edit this store" within ".actions"
    Then I fill in "Title" with "A different title"
    And I press "Save"
    Then I should see "'A different title' was successfully updated."
    And I should be on the list of stores
    And I should not see "A title"

  @stores-duplicate @duplicate
  Scenario: Create Duplicate Store
    Given I only have stores titled UniqueTitleOne, UniqueTitleTwo
    When I go to the list of stores
    And I follow "Add New Store"
    And I fill in "Title" with "UniqueTitleTwo"
    And I press "Save"
    Then I should see "There were problems"
    And I should have 2 stores

  @stores-delete @delete
  Scenario: Delete Store
    Given I only have stores titled UniqueTitleOne
    When I go to the list of stores
    And I follow "Remove this store forever"
    Then I should see "'UniqueTitleOne' was successfully removed."
    And I should have 0 stores
 