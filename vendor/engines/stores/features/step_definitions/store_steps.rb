Given /^I have no stores$/ do
  Store.delete_all
end

Given /^I (only )?have stores titled "?([^"]*)"?$/ do |only, titles|
  Store.delete_all if only
  titles.split(', ').each do |title|
    Store.create(:title => title)
  end
end

Then /^I should have ([0-9]+) stores?$/ do |count|
  Store.count.should == count.to_i
end
