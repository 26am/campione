module NavigationHelpers
  module Refinery
    module Stores
      def path_to(page_name)
        case page_name
        when /the list of stores/
          admin_stores_path

         when /the new store form/
          new_admin_store_path
        else
          nil
        end
      end
    end
  end
end
