# Stores engine for Refinery CMS.

## How to build this engine as a gem

    cd vendor/engines/stores
    gem build refinerycms-stores.gempspec
    gem install refinerycms-stores.gem
    
    # Sign up for a http://rubygems.org/ account and publish the gem
    gem push refinerycms-stores.gem